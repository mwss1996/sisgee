$(document).ready(function(){
	$("input[name$='isAgenteIntegracao']").click(function() {
		 if ($(this).val() == 1) {
			$(".trueIsAgenteIntegracao").show();
			$(".falseIsAgenteIntegracao").hide();
		}
		 if ($(this).val() == 0) {
			$(".trueIsAgenteIntegracao").hide();
			$(".falseIsAgenteIntegracao").show();
		}
	 });
	
	$(function(){
	    $('#buscarCnpjEmpresa').click(function() {
	    	var cnpjEmpresa = $('#cnpjEmpresa').val();
			$.ajax({
				url: '/Sisgee/Controller?command=BuscarEmpresa&cnpjEmpresa=' + cnpjEmpresa,
		        datatype: 'JSON',
				success: function(data) {
					$('#razaoSocialEmpresa').val(data.nomeEmpresa);
					$('#idEmpresa').val(data.idEmpresa);
				},
				error: function(jqXHR, exception) {
			    	alert("Exception: " + exception + "; Status: " + jqXHR.status);
					$('#razaoSocialEmpresa').val("");
					$('#idEmpresa').val("");
				},
			});
	    });
	});

	$(function(){
	    $('#buscarCnpjEmpresaLigadaAgenteIntegracao').click(function() {
	    	var cnpjEmpresa = $('#cnpjEmpresaLigadaAgenteIntegracao').val();
	    	var razaoSocialAgenteIntegracao = $('#razaoSocialAgenteIntegracao').val();
			$.ajax({
				url: '/Sisgee/Controller?command=BuscarEmpresaLigadaAgenteIntegracao&cnpjEmpresa=' + cnpjEmpresa + "&idAgenteIntegracao=" + razaoSocialAgenteIntegracao,
		        datatype: 'JSON',
				success: function(data) {
					$('#razaoSocialEmpresaLigadaAgenteIntegracao').val(data.nomeEmpresa);
					$('#idEmpresaLigadaAgenteIntegracao').val(data.idEmpresa);
				},
				error: function(jqXHR, exception) {
			    	alert("Exception: " + exception + "; Status: " + jqXHR.status);
					$('#razaoSocialEmpresaLigadaAgenteIntegracao').val("");
					$('#idEmpresaLigadaAgenteIntegracao').val("");
				},
			});
	    });
	});
	
	$(function(){
	    $('#buscarMatriculaAluno').click(function() {
	    	var matriculaAluno = $('#matriculaAluno').val();
			$.ajax({
				url: '/Sisgee/Controller?command=BuscarAluno&matriculaAluno=' + matriculaAluno,
		        datatype: 'JSON',
				success: function(data) {
					$('#nomeAluno').val(data.pessoa.nome);
					$('#cursoAluno').val(data.curso.nomeCurso);
					$('#unidadeAluno').val(data.curso.campus.nomeCampus);
					$('#idAluno').val(data.idAluno);
				},
				error: function(jqXHR, exception) {
			    	alert("Exception: " + exception + "; Status: " + jqXHR.status);
					$('#nomeAluno').val("");
					$('#cursoAluno').val("");
					$('#unidadeAluno').val("");
					$('#idAluno').val("");
				},
			});
	    });
	});
 });

