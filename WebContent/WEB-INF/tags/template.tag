<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ attribute name="head" fragment="true" %>
<%@ attribute name="page" %>
<!DOCTYPE html>
<html>
	<head>
		<title>SISGEE<c:if test="${not empty page}"> - ${page}</c:if></title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/style.css"/>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/color.css"/>
		<script src="${pageContext.request.contextPath}/resource/jquery-3.2.1.min.js"></script>
		<script src="${pageContext.request.contextPath}/resource/jquery.mask.min.js"></script>
		<script src="${pageContext.request.contextPath}/resource/mask.js"></script>
		<meta charset="UTF-8" name="viewport" content="width = device-width, initial-scale = 1.0"/>
		<jsp:invoke fragment="head"/>
	</head>
	<body>
		<header class="clearfix">
			<div class="width">
				<h1><a href="${pageContext.request.contextPath}/">SISGEE</a></h1>
				<ul>
					<li><span><fmt:message key="estagio"/></span>
						<ul>
							<li><a href="${pageContext.request.contextPath}/Controller?command=RegistrarTermo"><fmt:message key="registrar_termo"/></a></li>
							<li><a href="${pageContext.request.contextPath}/Controller?command=RegistrarAditivo"><fmt:message key="registro_do_aditivo"/></a></li>
							<li><a href="${pageContext.request.contextPath}/Controller?command=RegistrarRescisao"><fmt:message key="registro_de_recisao"/></a></li>
							<li><a href="${pageContext.request.contextPath}/Controller?command=RelatorioConsolidado"><fmt:message key="relatorio_consolidado"/></a></li>
							<li><a href="${pageContext.request.contextPath}/Controller?command=RegistrarEmpresa"><fmt:message key="cadastrar_empresa"/></a></li>
						</ul>
					</li>
				</ul>
			</div>
		</header>
		<div class="width clearfix">
			<main>
				<jsp:doBody/>
			</main>
		</div>
		<%-- <footer><img src="https://cli.angular.io/node_modules/microsite-ui/images/angular-logo.svg" alt="" /></footer> --%>
	</body>
</html>