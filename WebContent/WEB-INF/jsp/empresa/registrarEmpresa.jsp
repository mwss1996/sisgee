<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="bookstore" tagdir="/WEB-INF/tags" %>
<bookstore:template page="Registro de Empresa">
   <jsp:attribute name="head">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/form.css"/>
		<script src="${pageContext.request.contextPath}/resource/registrarTermo.js"></script> 
	</jsp:attribute>
	<jsp:body>
		<section>
			<article class="formulario">
				<h1>Registro de Empresa (ou Agente de Integra��o)</h1>
				<c:if test="${not empty message}">
					<div class="message">${message}</div>
				</c:if>
				<form action="${pageContext.request.contextPath}/Controller?command=RegistrarEmpresa" method="POST">
					<table cellspacing="5">
						<tr>
							<td>
								<label for="isAgenteIntegracao">� agente de integra��o?</label>
							</td>
							<td>
								<input type="radio" name="isAgenteIntegracao" value="1" checked/> Sim
								<input type="radio" name="isAgenteIntegracao" value="0"/> N�o
							</td>
						</tr>
						<tr class="falseIsAgenteIntegracao">
							<td><label for="razaoSocialAgenteIntegracao">Agente de Integra��o</label></td>
							<td>
								<select name="razaoSocialAgenteIntegracao">
									<c:forEach var="agenteIntegracao" items="${listAgenteIntegracao}">
										<option value="${agenteIntegracao.idAgenteIntegracao}">${agenteIntegracao.nomeAgenteIntegracao}</option>
									</c:forEach>
								</select>
							</td>
						</tr>
						<tr>
							<td><label for="cnpjEmpresa">CNPJ</label></td>
							<td><input type="text" class="cnpj" name="cnpjEmpresa" /></td>
						</tr>
						<tr>
							<td><label for="razaoSocialEmpresa">Raz�o Social</label></td>
							<td><input type="text" name="razaoSocialEmpresa" /></td>
						</tr>
					</table>
					<input type="submit" value="salvar"/>
				</form>
			</article>
		</section>
	</jsp:body>
</bookstore:template>