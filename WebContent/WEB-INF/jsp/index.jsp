<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bookstore" tagdir="/WEB-INF/tags" %>
<bookstore:template>
	<jsp:body>
		<section>
			<article>
				<h1>Trabalho Final da mat�ria de Programa��o de Software Web</h1>
				<h3>Grupo</h3>
				<ul>
					<li>Daniel Moreira</li>
					<li>Giovanni Duarte</li>
					<li>Francisco Chernicharo</li>
					<li>Michael Wallace</li>
				</ul>
			</article>
		</section>
	</jsp:body>
</bookstore:template>