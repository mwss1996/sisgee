<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="e" uri="cause"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="pos" %>
<%@ page isErrorPage="true" %>
<pos:template page="Error">
	<jsp:body>
		<section>
			<article>
				<h1>Error</h1>
				<p>Requested URI: ${pageContext.errorData.requestURI}</p>
				<p>Status Code: <a target="_blank" href="https://httpstatuses.com/${pageContext.errorData.statusCode}">${pageContext.errorData.statusCode}</a></p>     
				<h2>Cause</h2>
				<e:cause exception="${pageContext.exception}">
					<p><b>${class}</b>: ${message}</p>
				</e:cause>
			</article>
		</section>
	</jsp:body>
</pos:template>