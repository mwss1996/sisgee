<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="bookstore" tagdir="/WEB-INF/tags" %>
<bookstore:template page="Registro do Termo de Rescis�o">
   <jsp:attribute name="head">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/form.css"/>
		<script src="${pageContext.request.contextPath}/resource/registrarTermo.js"></script> 
	</jsp:attribute>
	<jsp:body>
		<section>
			<article class="formulario">
				<h1>Registro do Termo de Rescis�o</h1>
				<c:if test="${not empty message}">
					<div class="message">${message}</div>
				</c:if>
				<form action="${pageContext.request.contextPath}/Controller?command=RegistrarRescisao" method="POST">
					<div>
						<h2>Dados do Aluno</h2>
						<table cellspacing="5">
							<tr>
								<td><label for="matriculaAluno">Matr�cula</label></td>
								<td><button type="button" id="buscarMatriculaAluno">Buscar</button><input type="text" id="matriculaAluno" name="matriculaAluno"/></td>
							</tr>
							<tr>
								<td><label for="nomeAluno">Nome</label></td>
								<td><input type="text" id="nomeAluno" name="nomeAluno" disabled/></td>
							</tr>
							<tr>
								<td><label for="cursoAluno">Curso</label></td>
								<td><input type="text" id="cursoAluno" name="cursoAluno" disabled/></td>
							</tr>
							<tr>
								<td><label for="unidadeAluno">Unidade</label></td>
								<td><input type="text" id="unidadeAluno" name="unidadeAluno" disabled/></td>
							</tr>
						</table>
						<input type="hidden" id="idAluno" name="idAluno"/>
					</div>
					<div>
						<h2>Rescis�o</h2>
						<table cellspacing="5">
							<tr>
								<td><label for="dataRescisao">Data de Rescis�o</label></td>
								<td><input type="text" class="date" name="dataRescisao"/></td>
							</tr>
						</table>
					</div>
					<input type="submit" value="salvar"/>
					
				</form>
			</article>
		</section>
	</jsp:body>
</bookstore:template>