<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="bookstore" tagdir="/WEB-INF/tags" %>
<bookstore:template page="Relat�rio Consolidado">
   <jsp:attribute name="head">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/form.css"/>
		<script src="${pageContext.request.contextPath}/resource/registrarTermo.js"></script> 
	</jsp:attribute>
	<jsp:body>
		<section>
			<article class="formulario">
				<h1>Relat�rio Consolidado</h1>
				<form action="${pageContext.request.contextPath}/Controller?command=RelatorioConsolidado" method="POST">
					<div>
						<h2>Crit�rios de Pesquisa</h2>
						<table cellspacing="5">
							<tr>
								<td><label for="dataInicio">Data de In�cio</td>
								<td><input type="text" class="date" name="dataInicio"/></td>
							</tr>
							<tr>
								<td><label for="dataTermino">Data de T�rmino</label></td>
								<td><input type="text" class="date" name="dataTermino"/></td>
							</tr>
							<tr>
								<td>
									<label for="isEstagioObrigatorio">O est�gio � obrigat�rio?</label>
								</td>
								<td>
									<input type="radio" name="isEstagioObrigatorio" value="1" checked/> Sim
									<input type="radio" name="isEstagioObrigatorio" value="0"/> N�o
								</td>
							</tr>
						</table>
					</div>
					<input type="submit" value="buscar"/>
				</form>
				<c:if test="${not empty relatorios}">
				<h2>Resultados da Pesquisa</h2>
					<div>
						<c:forEach var="relatorio" items="${relatorios}">
							<h3>${relatorio.curso}</h3>
							<table cellspacing="5">
								<tr>
									<td><label>Termos de Estagio Registrados</label></td>
									<td><label>${relatorio.termosDeEstagioRegistrados}</label></td>
								</tr>
								<tr>
									<td><label>Aditivos Registrados</label></td>
									<td><label>${relatorio.aditivosRegistrados}</label></td>
								</tr>
								<tr>
									<td><label>Recisoes Registradas</label></td>
									<td><label>${relatorio.recisoesRegistradas}</label></td>
								</tr>
							</table>
						</c:forEach>
					</div>
				</c:if>
			</article>
		</section>
	</jsp:body>
</bookstore:template>