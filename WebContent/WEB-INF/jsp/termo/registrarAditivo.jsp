<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="bookstore" tagdir="/WEB-INF/tags" %>
<bookstore:template page="Registrar Termo Aditivo">
   <jsp:attribute name="head">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/form.css"/>
		<script src="${pageContext.request.contextPath}/resource/registrarTermo.js"></script> 
	</jsp:attribute>
	<jsp:body>
		<section>
			<article class="formulario">
				<h1>Registrar Termo Aditivo</h1>
				<c:if test="${not empty message}">
					<div class="message">${message}</div>
				</c:if>
				<form action="${pageContext.request.contextPath}/Controller?command=RegistrarAditivo" method="POST">
					<div>
						<h2>Dados do Aluno</h2>
						<table cellspacing="5">
							<tr>
								<td><label for="matriculaAluno">Matr�cula</label></td>
								<td><button type="button" id="buscarMatriculaAluno">Buscar</button><input type="text" id="matriculaAluno" name="matriculaAluno"/></td>
							</tr>
							<tr>
								<td><label for="nomeAluno">Nome</label></td>
								<td><input type="text" id="nomeAluno" name="nomeAluno" disabled/></td>
							</tr>
							<tr>
								<td><label for="cursoAluno">Curso</label></td>
								<td><input type="text" id="cursoAluno" name="cursoAluno" disabled/></td>
							</tr>
							<tr>
								<td><label for="unidadeAluno">Unidade</label></td>
								<td><input type="text" id="unidadeAluno" name="unidadeAluno" disabled/></td>
							</tr>
						</table>
						<input type="hidden" id="idAluno" name="idAluno"/>
					</div>
					<div>
						<h2>Vig�ncia do Est�gio</h2>
						<table cellspacing="5">
							<tr>
								<td><label for="dataTermino">Data de T�rmino</label></td>
								<td><input type="text" class="date" name="dataTermino"/></td>
							</tr>
						</table>
					</div>
					<div>
						<h2>Carga Hor�ria do Aluno</h2>
						<table cellspacing="5">
							<tr>
								<td><label for="cargaHoraria">Horas por dia</label></td>
								<td><input type="text" class="twoDigit" name="cargaHoraria"/></td>
							</tr>
						</table>
					</div>
					<div>
						<h2>Valor da Bolsa de Est�gio</h2>
						<table cellspacing="5">
							<tr>
								<td><label for="valorBolsa">Valor</label></td>
								<td><input type="text" class="money" name="valorBolsa"/></td>
							</tr>
						</table>
					</div>
					<div>
						<h2>Local do Est�gio</h2>
						<table cellspacing="5">
							<tr>
								<td><label for="enderecoTermoAditivo">Endere�o</label></td>
								<td><input type="text" name="enderecoTermoAditivo" /></td>
								<td><label for="numeroEnderecoTermoAditivo">N�mero</label></td>
								<td><input type="text" name="numeroEnderecoTermoAditivo" /></td>
							</tr>
							<tr>
								<td><label for="complementoEnderecoTermoAditivo">Complemento</label></td>
								<td><input type="text" name="complementoEnderecoTermoAditivo" /></td>
							</tr>
							<tr>
								<td><label for="bairroEnderecoTermoAditivo">Bairro</label></td>
								<td><input type="text" name="bairroEnderecoTermoAditivo" /></td>
								<td><label for="cidadeEnderecoTermoAditivo">Cidade</label></td>
								<td><input type="text" name="cidadeEnderecoTermoAditivo" /></td>
							</tr>
							<tr>
								<td><label for="estadoEnderecoTermoAditivo">Estado</label></td>
								<td>
									<select name="estadoEnderecoTermoAditivo" id="">
										<option value="RJ">RJ</option>
										<option value="AC">AC</option>
										<option value="AL">AL</option>
										<option value="AP">AP</option>
										<option value="AM">AM</option>
										<option value="BA">BA</option>
										<option value="CE">CE</option>
										<option value="DF">DF</option>
										<option value="ES">ES</option>
										<option value="GO">GO</option>
										<option value="MA">MA</option>
										<option value="MT">MT</option>
										<option value="MS">MS</option>
										<option value="MG">MG</option>
										<option value="PA">PA</option>
										<option value="PB">PB</option>
										<option value="PR">PR</option>
										<option value="PE">PE</option>
										<option value="PI">PI</option>
										<option value="RN">RN</option>
										<option value="RS">RS</option>
										<option value="RO">RO</option>
										<option value="RR">RR</option>
										<option value="SC">SC</option>
										<option value="SP">SP</option>
										<option value="SE">SE</option>
										<option value="TO">TO</option>
									</select>
								</td>
								<td><label for="cepEnderecoTermoAditivo">CEP</label></td>
								<td><input type="text" class="cep" name="cepEnderecoTermoAditivo" /></td>
							</tr>
						</table>
					</div>
					<input type="submit" value="salvar"/>
				</form>
			</article>
		</section>
	</jsp:body>
</bookstore:template>