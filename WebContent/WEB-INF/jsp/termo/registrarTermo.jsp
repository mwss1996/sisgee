<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="bookstore" tagdir="/WEB-INF/tags" %>
<bookstore:template page="Registrar Termo">
   <jsp:attribute name="head">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/form.css"/>
		<script src="${pageContext.request.contextPath}/resource/registrarTermo.js"></script> 
	</jsp:attribute>
	<jsp:body>
		<section>
			<article class="formulario">
				<h1>Registrar Termo</h1>
				<c:if test="${not empty message}">
					<div class="message">${message}</div>
				</c:if>
				<form action="${pageContext.request.contextPath}/Controller?command=RegistrarTermo" method="POST">
					<div>
						<h2>Dados da Empresa Conveniada</h2>
						<table cellspacing="5">
							<tr>
								<td>
									<label for="numeroConvenio">N�mero do Conv�nio</label>
								</td>
								<td>
									<input type="text" name="numeroConvenio"/>
								</td>
							</tr>
							<tr>
								<td>
									<label for="isAgenteIntegracao">� agente de integra��o?</label>
								</td>
								<td>
									<input type="radio" name="isAgenteIntegracao" value="1" checked/> Sim
									<input type="radio" name="isAgenteIntegracao" value="0"/> N�o
								</td>
							</tr>
							<tr class="trueIsAgenteIntegracao">
								<td><label for="razaoSocialAgenteIntegracao">Raz�o Social</label></td>
								<td>
									<select name="razaoSocialAgenteIntegracao" id="razaoSocialAgenteIntegracao">
										<c:forEach var="agenteIntegracao" items="${listAgenteIntegracao}">
											<option value="${agenteIntegracao.idAgenteIntegracao}">${agenteIntegracao.nomeAgenteIntegracao}</option>
										</c:forEach>
									</select>
								</td>
							</tr>
							<tr class="trueIsAgenteIntegracao">
								<td><label for="cnpjEmpresaLigadaAgenteIntegracao">CNPJ da Empresa Ligada ao Agente de Integra��o</label></td>
								<td><button type="button" id="buscarCnpjEmpresaLigadaAgenteIntegracao">Buscar</button><input type="text" class="cnpj" id="cnpjEmpresaLigadaAgenteIntegracao" name="cnpjEmpresaLigadaAgenteIntegracao"/></td>
								<td><label for="razaoSocialEmpresaLigadaAgenteIntegracao">Raz�o Social da Empresa Ligada ao Agente de Integra��o</label></td>
								<td><input type="text" id="razaoSocialEmpresaLigadaAgenteIntegracao" name="razaoSocialEmpresaLigadaAgenteIntegracao"  disabled/></td>
							</tr>
							<tr class="falseIsAgenteIntegracao">
								<td><label for="cnpj">CNPJ</label></td>
								<td><button type="button" id="buscarCnpjEmpresa">Buscar</button><input type="text" class="cnpj" id="cnpjEmpresa" name="cnpjEmpresa"/></td>
								<td><label for="razaoSocialEmpresa">Raz�o Social</label></td>
								<td><input type="text" id="razaoSocialEmpresa" name="razaoSocialEmpresa"  disabled/></td>
							</tr>
						</table>
						<input type="hidden" id="idEmpresaLigadaAgenteIntegracao" name="idEmpresaLigadaAgenteIntegracao" value=""/>
						<input type="hidden" id="idEmpresa" name="idEmpresa" value=""/>
					</div>
					<div>
						<h2>Dados do Aluno</h2>
						<table cellspacing="5">
							<tr>
								<td><label for="matriculaAluno">Matr�cula</label></td>
								<td><button type="button" id="buscarMatriculaAluno">Buscar</button><input type="text" id="matriculaAluno" name="matriculaAluno"/></td>
							</tr>
							<tr>
								<td><label for="nomeAluno">Nome</label></td>
								<td><input type="text" id="nomeAluno" name="nomeAluno" disabled/></td>
							</tr>
							<tr>
								<td><label for="cursoAluno">Curso</label></td>
								<td><input type="text" id="cursoAluno" name="cursoAluno" disabled/></td>
							</tr>
							<tr>
								<td><label for="unidadeAluno">Unidade</label></td>
								<td><input type="text" id="unidadeAluno" name="unidadeAluno" disabled/></td>
							</tr>
						</table>
						<input type="hidden" id="idAluno" name="idAluno"/>
					</div>
					<div>
						<h2>Vig�ncia do Est�gio</h2>
						<table cellspacing="5">
							<tr>
								<td><label for="dataInicio">Data de In�cio</td>
								<td><input type="text" class="date" name="dataInicio"/></td>
							</tr>
							<tr>
								<td><label for="dataTermino">Data de T�rmino</label></td>
								<td><input type="text" class="date" name="dataTermino"/></td>
							</tr>
						</table>
					</div>
					<div>
						<h2>Carga Hor�ria do Aluno</h2>
						<table cellspacing="5">
							<tr>
								<td><label for="cargaHoraria">Horas por dia</label></td>
								<td><input type="text" class="twoDigit" name="cargaHoraria"/></td>
							</tr>
						</table>
					</div>
					<div>
						<h2>Valor da Bolsa de Est�gio</h2>
						<table cellspacing="5">
							<tr>
								<td><label for="valorBolsa">Valor</label></td>
								<td><input type="text" class="money" name="valorBolsa"/></td>
							</tr>
						</table>
					</div>
					<div>
						<h2>Local do Est�gio</h2>
						<table cellspacing="5">
							<tr>
								<td><label for="enderecoTermoEstagio">Endere�o</label></td>
								<td><input type="text" name="enderecoTermoEstagio" /></td>
								<td><label for="numeroEnderecoTermoEstagio">N�mero</label></td>
								<td><input type="text" name="numeroEnderecoTermoEstagio" /></td>
							</tr>
							<tr>
								<td><label for="complementoEnderecoTermoEstagio">Complemento</label></td>
								<td><input type="text" name="complementoEnderecoTermoEstagio" /></td>
							</tr>
							<tr>
								<td><label for="bairroEnderecoTermoEstagio">Bairro</label></td>
								<td><input type="text" name="bairroEnderecoTermoEstagio" /></td>
								<td><label for="cidadeEnderecoTermoEstagio">Cidade</label></td>
								<td><input type="text" name="cidadeEnderecoTermoEstagio" /></td>
							</tr>
							<tr>
								<td><label for="estadoEnderecoTermoEstagio">Estado</label></td>
								<td>
									<select name="estadoEnderecoTermoEstagio" id="">
										<option value="RJ">RJ</option>
										<option value="AC">AC</option>
										<option value="AL">AL</option>
										<option value="AP">AP</option>
										<option value="AM">AM</option>
										<option value="BA">BA</option>
										<option value="CE">CE</option>
										<option value="DF">DF</option>
										<option value="ES">ES</option>
										<option value="GO">GO</option>
										<option value="MA">MA</option>
										<option value="MT">MT</option>
										<option value="MS">MS</option>
										<option value="MG">MG</option>
										<option value="PA">PA</option>
										<option value="PB">PB</option>
										<option value="PR">PR</option>
										<option value="PE">PE</option>
										<option value="PI">PI</option>
										<option value="RN">RN</option>
										<option value="RS">RS</option>
										<option value="RO">RO</option>
										<option value="RR">RR</option>
										<option value="SC">SC</option>
										<option value="SP">SP</option>
										<option value="SE">SE</option>
										<option value="TO">TO</option>
									</select>
								</td>
								<td><label for="cepEnderecoTermoEstagio">CEP</label></td>
								<td><input type="text" class="cep" name="cepEnderecoTermoEstagio" /></td>
							</tr>
						</table>
					</div>
					<div id="outros">
						<table cellspacing="5">
							<tr>
								<td>
									<label for="isEstagioObrigatorio">O est�gio � obrigat�rio?</label>
								</td>
								<td>
									<input type="radio" name="isEstagioObrigatorio" value="1" checked/> Sim
									<input type="radio" name="isEstagioObrigatorio" value="0"/> N�o
								</td>
							</tr>
							<tr>
								<td><label for="professorOrientador">Professor Orientador</label></td>
								<td>
									<select name="professorOrientador">
										<c:forEach var="professorOrientador" items="${listProfessorOrientador}">
											<option value="${professorOrientador.idProfessorOrientador}">${professorOrientador.nomeProfessorOrientador}</option>
										</c:forEach>
									</select>
								</td>
							</tr>
						</table>
					</div>
					<input type="submit" value="salvar"/>
				</form>
			</article>
		</section>
	</jsp:body>
</bookstore:template>