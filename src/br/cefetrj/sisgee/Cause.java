package br.cefetrj.sisgee;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class Cause extends SimpleTagSupport {
	
	private Throwable exception;
	
	public void setException(Throwable exception) {
		this.exception = exception;
	}

	@Override
	public void doTag() throws IOException, JspException {
		while (exception.getCause() != null) {
			String[] split = exception.getCause().getClass().getName().split("\\.");
			getJspContext().setAttribute("class", split[split.length - 1]);
			getJspContext().setAttribute("message", exception.getCause().getMessage());
			getJspBody().invoke(null);
			exception = exception.getCause();
		}
	}
}
