package br.cefetrj.sisgee.control;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.cefetrj.sisgee.commands.Command;

/**
 * Controller para Gerenciar requisiçoes
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

@WebServlet("/Controller")
public class Controller extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Command command = (Command) Class.forName("br.cefetrj.sisgee.commands." + request.getParameter("command")).newInstance();
			command.execute(request, response);
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
	}
}