package br.cefetrj.sisgee.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Classe da entidade TermoAditivo
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

@Entity
public class TermoAditivo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idTermoAditivo;
	
	private Date dataFimTermoAditivo;
	private short cargaHorariaTermoAditivo;
	private float valorBolsaTermoAditivo;
	private String enderecoTermoAditivo;
	private String numeroEnderecoTermoAditivo;
	private String complementoEnderecoTermoEstagio;
	private String bairroEnderecoTermoAditivo;
	private String cepEnderecoTermoAditivo;
	private String cidadeEnderecoTermoAditivo;
	private String estadoEnderecoTermoAditivo;
	
	@ManyToOne @JoinColumn(name="idTermoEstagio")
	private TermoEstagio termoEstagio;
	
	public Date getDataFimTermoAditivo() {
		return dataFimTermoAditivo;
	}
	
	public void setDataFimTermoAditivo(Date dataFimTermoAditivo) {
		this.dataFimTermoAditivo = dataFimTermoAditivo;
	}
	
	public short getCargaHorariaTermoAditivo() {
		return cargaHorariaTermoAditivo;
	}
	
	public void setCargaHorariaTermoAditivo(short cargaHorariaTermoAditivo) {
		this.cargaHorariaTermoAditivo = cargaHorariaTermoAditivo;
	}
	
	public float getValorBolsaTermoAditivo() {
		return valorBolsaTermoAditivo;
	}
	
	public void setValorBolsaTermoAditivo(float valorBolsaTermoAditivo) {
		this.valorBolsaTermoAditivo = valorBolsaTermoAditivo;
	}
	
	public String getEnderecoTermoAditivo() {
		return enderecoTermoAditivo;
	}
	
	public void setEnderecoTermoAditivo(String enderecoTermoAditivo) {
		this.enderecoTermoAditivo = enderecoTermoAditivo;
	}
	
	public String getNumeroEnderecoTermoAditivo() {
		return numeroEnderecoTermoAditivo;
	}
	
	public void setNumeroEnderecoTermoAditivo(String numeroEnderecoTermoAditivo) {
		this.numeroEnderecoTermoAditivo = numeroEnderecoTermoAditivo;
	}
	
	public String getComplementoEnderecoTermoEstagio() {
		return complementoEnderecoTermoEstagio;
	}
	
	public void setComplementoEnderecoTermoEstagio(String complementoEnderecoTermoEstagio) {
		this.complementoEnderecoTermoEstagio = complementoEnderecoTermoEstagio;
	}
	
	public String getBairroEnderecoTermoAditivo() {
		return bairroEnderecoTermoAditivo;
	}
	
	public void setBairroEnderecoTermoAditivo(String bairroEnderecoTermoAditivo) {
		this.bairroEnderecoTermoAditivo = bairroEnderecoTermoAditivo;
	}
	
	public String getCepEnderecoTermoAditivo() {
		return cepEnderecoTermoAditivo;
	}
	
	public void setCepEnderecoTermoAditivo(String cepEnderecoTermoAditivo) {
		this.cepEnderecoTermoAditivo = cepEnderecoTermoAditivo;
	}
	
	public String getCidadeEnderecoTermoAditivo() {
		return cidadeEnderecoTermoAditivo;
	}
	
	public void setCidadeEnderecoTermoAditivo(String cidadeEnderecoTermoAditivo) {
		this.cidadeEnderecoTermoAditivo = cidadeEnderecoTermoAditivo;
	}
	
	public String getEstadoEnderecoTermoAditivo() {
		return estadoEnderecoTermoAditivo;
	}
	
	public void setEstadoEnderecoTermoAditivo(String estadoEnderecoTermoAditivo) {
		this.estadoEnderecoTermoAditivo = estadoEnderecoTermoAditivo;
	}
	
	public TermoEstagio getTermoEstagio() {
		return termoEstagio;
	}
	
	public void setTermoEstagio(TermoEstagio termoEstagio) {
		this.termoEstagio = termoEstagio;
	}
	
	public Long getIdTermoAditivo() {
		return idTermoAditivo;
	}
}
