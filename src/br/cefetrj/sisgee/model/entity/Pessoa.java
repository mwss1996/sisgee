package br.cefetrj.sisgee.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Classe da entidade Pessoa
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

@Entity
public class Pessoa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idPessoa;

	private String cpf;
	private String nome;
	private Date dataNascimento;
	private String tipoEndereco;
	private String endereco;
	private String numeroEndereco;
	private String complementoEndereco;
	private String bairroEndereco;
	private String cepEndereco;
	private String distritoEndereco;
	private String cidadeEndereco;
	private String estadoEndereco;
	private String paisEndereco;
	private String email;
	private Integer ddiResidencial;
	private Integer dddResidencial;
	private Integer telefoneResidencial;
	private Integer ddiComercial;
	private Integer dddComercial;
	private Integer telefoneComercial;
	private Integer ddiCelular;
	private Integer dddCelular;
	
	public Pessoa() {}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getTipoEndereco() {
		return tipoEndereco;
	}

	public void setTipoEndereco(String tipoEndereco) {
		this.tipoEndereco = tipoEndereco;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumeroEndereco() {
		return numeroEndereco;
	}

	public void setNumeroEndereco(String numeroEndereco) {
		this.numeroEndereco = numeroEndereco;
	}

	public String getComplementoEndereco() {
		return complementoEndereco;
	}

	public void setComplementoEndereco(String complementoEndereco) {
		this.complementoEndereco = complementoEndereco;
	}

	public String getBairroEndereco() {
		return bairroEndereco;
	}

	public void setBairroEndereco(String bairroEndereco) {
		this.bairroEndereco = bairroEndereco;
	}

	public String getCepEndereco() {
		return cepEndereco;
	}

	public void setCepEndereco(String cepEndereco) {
		this.cepEndereco = cepEndereco;
	}

	public String getDistritoEndereco() {
		return distritoEndereco;
	}

	public void setDistritoEndereco(String distritoEndereco) {
		this.distritoEndereco = distritoEndereco;
	}

	public String getCidadeEndereco() {
		return cidadeEndereco;
	}

	public void setCidadeEndereco(String cidadeEndereco) {
		this.cidadeEndereco = cidadeEndereco;
	}

	public String getEstadoEndereco() {
		return estadoEndereco;
	}

	public void setEstadoEndereco(String estadoEndereco) {
		this.estadoEndereco = estadoEndereco;
	}

	public String getPaisEndereco() {
		return paisEndereco;
	}

	public void setPaisEndereco(String paisEndereco) {
		this.paisEndereco = paisEndereco;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getDdiResidencial() {
		return ddiResidencial;
	}

	public void setDdiResidencial(Integer ddiResidencial) {
		this.ddiResidencial = ddiResidencial;
	}

	public Integer getDddResidencial() {
		return dddResidencial;
	}

	public void setDddResidencial(Integer dddResidencial) {
		this.dddResidencial = dddResidencial;
	}

	public Integer getTelefoneResidencial() {
		return telefoneResidencial;
	}

	public void setTelefoneResidencial(Integer telefoneResidencial) {
		this.telefoneResidencial = telefoneResidencial;
	}

	public Integer getDdiComercial() {
		return ddiComercial;
	}

	public void setDdiComercial(Integer ddiComercial) {
		this.ddiComercial = ddiComercial;
	}

	public Integer getDddComercial() {
		return dddComercial;
	}

	public void setDddComercial(Integer dddComercial) {
		this.dddComercial = dddComercial;
	}

	public Integer getTelefoneComercial() {
		return telefoneComercial;
	}

	public void setTelefoneComercial(Integer telefoneComercial) {
		this.telefoneComercial = telefoneComercial;
	}

	public Integer getDdiCelular() {
		return ddiCelular;
	}

	public void setDdiCelular(Integer ddiCelular) {
		this.ddiCelular = ddiCelular;
	}

	public Integer getDddCelular() {
		return dddCelular;
	}

	public void setDddCelular(Integer dddCelular) {
		this.dddCelular = dddCelular;
	}

	public Long getIdPessoa() {
		return idPessoa;
	}
}
