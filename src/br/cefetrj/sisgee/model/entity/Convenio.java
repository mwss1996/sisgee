package br.cefetrj.sisgee.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Classe da entidade Convenio
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

@Entity
public class Convenio {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idConvenio;

	private String numeroConvenio;
	private Date dataInicioConvenio;
	private Date dataFimConvenio;

	@ManyToOne @JoinColumn(name="idEmpresa") 
	private Empresa empresa;

	public Convenio() {}

	public Convenio(String numeroConvenio, Date dataInicioConvenio, Date dataFimConvenio, Empresa empresa) {
		super();
		this.numeroConvenio = numeroConvenio;
		this.dataInicioConvenio = dataInicioConvenio;
		this.dataFimConvenio = dataFimConvenio;
		this.empresa = empresa;
	}

	public String getNumeroConvenio() {
		return numeroConvenio;
	}

	public void setNumeroConvenio(String numeroConvenio) {
		this.numeroConvenio = numeroConvenio;
	}

	public Date getDataInicioConvenio() {
		return dataInicioConvenio;
	}

	public void setDataInicioConvenio(Date dataInicioConvenio) {
		this.dataInicioConvenio = dataInicioConvenio;
	}

	public Date getDataFimConvenio() {
		return dataFimConvenio;
	}

	public void setDataFimConvenio(Date dataFimConvenio) {
		this.dataFimConvenio = dataFimConvenio;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Long getIdConvenio() {
		return idConvenio;
	}
}