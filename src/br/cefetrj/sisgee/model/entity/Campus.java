package br.cefetrj.sisgee.model.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Classe da entidade Campus
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

@Entity
public class Campus {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCampus;

	private String nomeCampus;
	
	@OneToMany(mappedBy = "campus") @JsonIgnore
	private List<Curso> cursos;

	public Campus() {}

	public Campus(String nomeCampus) {
		this.nomeCampus = nomeCampus;
	}
	
	public Long getIdCampus() {
		return idCampus;
	}

	public String getNomeCampus() {
		return nomeCampus;
	}

	public void setNomeCampus(String nomeCampus) {
		this.nomeCampus = nomeCampus;
	}

	public List<Curso> getCursos() {
		return cursos;
	}
}
