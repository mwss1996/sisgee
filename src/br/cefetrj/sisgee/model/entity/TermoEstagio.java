package br.cefetrj.sisgee.model.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Classe da entidade TermoEstagio
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

@Entity
public class TermoEstagio {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idTermoEstagio;
	
	private Date dataInicioTermoEstagio;
	private Date dataFimTermoEstagio;
	private Date dataRescisaoTermoEstagio;
	private String situacaoTermoEstagio;
	private short cargaHorariaTermoEstagio;
	private float valorBolsa;
	private String enderecoTermoEstagio;
	private String numeroEnderecoTermoEstagio;
	private String complementoEnderecoTermoEstagio;
	private String bairroEnderecoTermoEstagio;
	private String cepEnderecoTermoEstagio;
	private String cidadeEnderecoTermoEstagio;
	private String estadoEnderecoTermoEstagio;
	private short eEstagioObrigatorio;

	@ManyToOne @JoinColumn(name="idAluno")
	private Aluno aluno;

	@ManyToOne @JoinColumn(name="idConvenio")
	private Convenio convenio;

	@ManyToOne @JoinColumn(name="idProfessorOrientador")
	private ProfessorOrientador professorOrientador;

	@OneToMany(mappedBy = "termoEstagio") @JsonIgnore
	private List<TermoAditivo> termosAditivos;
	
	public TermoEstagio() {}

	public Date getDataInicioTermoEstagio() {
		return dataInicioTermoEstagio;
	}

	public void setDataInicioTermoEstagio(Date dataInicioTermoEstagio) {
		this.dataInicioTermoEstagio = dataInicioTermoEstagio;
	}

	public Date getDataFimTermoEstagio() {
		return dataFimTermoEstagio;
	}

	public void setDataFimTermoEstagio(Date dataFimTermoEstagio) {
		this.dataFimTermoEstagio = dataFimTermoEstagio;
	}

	public Date getDataRescisaoTermoEstagio() {
		return dataRescisaoTermoEstagio;
	}

	public void setDataRescisaoTermoEstagio(Date dataRescisaoTermoEstagio) {
		this.dataRescisaoTermoEstagio = dataRescisaoTermoEstagio;
	}

	public String getSituacaoTermoEstagio() {
		return situacaoTermoEstagio;
	}

	public void setSituacaoTermoEstagio(String situacaoTermoEstagio) {
		this.situacaoTermoEstagio = situacaoTermoEstagio;
	}

	public short getCargaHorariaTermoEstagio() {
		return cargaHorariaTermoEstagio;
	}

	public void setCargaHorariaTermoEstagio(short cargaHorariaTermoEstagio) {
		this.cargaHorariaTermoEstagio = cargaHorariaTermoEstagio;
	}

	public float getValorBolsa() {
		return valorBolsa;
	}

	public void setValorBolsa(float valorBolsa) {
		this.valorBolsa = valorBolsa;
	}

	public String getEnderecoTermoEstagio() {
		return enderecoTermoEstagio;
	}

	public void setEnderecoTermoEstagio(String enderecoTermoEstagio) {
		this.enderecoTermoEstagio = enderecoTermoEstagio;
	}

	public String getNumeroEnderecoTermoEstagio() {
		return numeroEnderecoTermoEstagio;
	}

	public void setNumeroEnderecoTermoEstagio(String numeroEnderecoTermoEstagio) {
		this.numeroEnderecoTermoEstagio = numeroEnderecoTermoEstagio;
	}

	public String getComplementoEnderecoTermoEstagio() {
		return complementoEnderecoTermoEstagio;
	}

	public void setComplementoEnderecoTermoEstagio(String complementoEnderecoTermoEstagio) {
		this.complementoEnderecoTermoEstagio = complementoEnderecoTermoEstagio;
	}

	public String getBairroEnderecoTermoEstagio() {
		return bairroEnderecoTermoEstagio;
	}

	public void setBairroEnderecoTermoEstagio(String bairroEnderecoTermoEstagio) {
		this.bairroEnderecoTermoEstagio = bairroEnderecoTermoEstagio;
	}

	public String getCepEnderecoTermoEstagio() {
		return cepEnderecoTermoEstagio;
	}

	public void setCepEnderecoTermoEstagio(String cepEnderecoTermoEstagio) {
		this.cepEnderecoTermoEstagio = cepEnderecoTermoEstagio;
	}

	public String getCidadeEnderecoTermoEstagio() {
		return cidadeEnderecoTermoEstagio;
	}

	public void setCidadeEnderecoTermoEstagio(String cidadeEnderecoTermoEstagio) {
		this.cidadeEnderecoTermoEstagio = cidadeEnderecoTermoEstagio;
	}

	public String getEstadoEnderecoTermoEstagio() {
		return estadoEnderecoTermoEstagio;
	}

	public void setEstadoEnderecoTermoEstagio(String estadoEnderecoTermoEstagio) {
		this.estadoEnderecoTermoEstagio = estadoEnderecoTermoEstagio;
	}

	public short geteEstagioObrigatorio() {
		return eEstagioObrigatorio;
	}

	public void seteEstagioObrigatorio(short eEstagioObrigatorio) {
		this.eEstagioObrigatorio = eEstagioObrigatorio;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public Long getIdTermoEstagio() {
		return idTermoEstagio;
	}

	public Convenio getConvenio() {
		return convenio;
	}

	public void setConvenio(Convenio convenio) {
		this.convenio = convenio;
	}

	public ProfessorOrientador getProfessorOrientador() {
		return professorOrientador;
	}

	public void setProfessorOrientador(ProfessorOrientador professorOrientador) {
		this.professorOrientador = professorOrientador;
	}

	public List<TermoAditivo> getTermosAditivos() {
		return termosAditivos;
	}
}