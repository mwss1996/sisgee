package br.cefetrj.sisgee.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Classe da entidade Empresa
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

@Entity
public class Empresa {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idEmpresa;
	
	private String cnpjEmpresa;
	private String nomeEmpresa;
	
	@ManyToOne @JoinColumn(name="idAgenteIntegracao") 
	private AgenteIntegracao agenteIntegracao;
	
	public Empresa() {}
	
	public Empresa(String cnpjEmpresa, String nomeEmpresa) {
		this.cnpjEmpresa = cnpjEmpresa;
		this.nomeEmpresa = nomeEmpresa;
	}
	
	public long getIdEmpresa() {
		return idEmpresa;
	}
	
	public void setIdEmpresa(long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	
	public AgenteIntegracao getAgenteIntegracao() {
		return agenteIntegracao;
	}
	
	public void setAgenteIntegracao(AgenteIntegracao agenteIntegracao) {
		this.agenteIntegracao = agenteIntegracao;
	}
	
	public String getCnpjEmpresa() {
		return cnpjEmpresa;
	}
	
	public void setCnpjEmpresa(String cnpjEmpresa) {
		this.cnpjEmpresa = cnpjEmpresa;
	}
	
	public String getNomeEmpresa() {
		return nomeEmpresa;
	}
	
	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}
}