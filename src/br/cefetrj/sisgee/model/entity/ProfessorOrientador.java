package br.cefetrj.sisgee.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Classe da entidade ProfessorOrientador
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

@Entity
public class ProfessorOrientador {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idProfessorOrientador;
	
	private String nomeProfessorOrientador;
	
	public ProfessorOrientador() {}
	
	public ProfessorOrientador(String nomeProfessorOrientador) {
		this.nomeProfessorOrientador = nomeProfessorOrientador;
	}
	
	public String getNomeProfessorOrientador() {
		return nomeProfessorOrientador;
	}
	
	public void setNomeProfessorOrientador(String nomeProfessorOrientador) {
		this.nomeProfessorOrientador = nomeProfessorOrientador;
	}
	
	public Long getIdProfessorOrientador() {
		return idProfessorOrientador;
	}
}