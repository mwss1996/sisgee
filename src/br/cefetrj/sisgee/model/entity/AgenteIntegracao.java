package br.cefetrj.sisgee.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Classe da entidade AgenteIntegracao
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

@Entity
public class AgenteIntegracao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idAgenteIntegracao;
	
	private String cnpjAgenteIntegracao;
	private String nomeAgenteIntegracao;
	
	public AgenteIntegracao() {}

	public Long getIdAgenteIntegracao() {
		return idAgenteIntegracao;
	}

	public void setIdAgenteIntegracao(Long idAgenteIntegracao) {
		this.idAgenteIntegracao = idAgenteIntegracao;
	}

	public AgenteIntegracao(String cnpjAgenteIntegracao, String nomeAgenteIntegracao) {
		this.cnpjAgenteIntegracao = cnpjAgenteIntegracao;
		this.nomeAgenteIntegracao = nomeAgenteIntegracao;
	}

	public String getCnpjAgenteIntegracao() {
		return cnpjAgenteIntegracao;
	}

	public void setCnpjAgenteIntegracao(String cnpjAgenteIntegracao) {
		this.cnpjAgenteIntegracao = cnpjAgenteIntegracao;
	}

	public String getNomeAgenteIntegracao() {
		return nomeAgenteIntegracao;
	}

	public void setNomeAgenteIntegracao(String nomeAgenteIntegracao) {
		this.nomeAgenteIntegracao = nomeAgenteIntegracao;
	}
}
