package br.cefetrj.sisgee.model.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Classe da entidade Aluno
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

@Entity
public class Aluno {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idAluno;

	private String matricula;
	private String situacao;

	@ManyToOne @JoinColumn(name="idPessoa") 
	private Pessoa pessoa;

	@ManyToOne @JoinColumn(name="idCurso") 
	private Curso curso;

	@OneToMany(mappedBy = "aluno") @JsonIgnore
	private List<TermoEstagio> termosEstagios;

	public Aluno() {}

	public Aluno(String matricula, String situacao, Pessoa pessoa, Curso curso) {
		super();
		this.matricula = matricula;
		this.situacao = situacao;
		this.pessoa = pessoa;
		this.curso = curso;
	}
	
	public Long getIdAluno() {
		return idAluno;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	
	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public List<TermoEstagio> getTermosEstagios() {
		return termosEstagios;
	}
}
