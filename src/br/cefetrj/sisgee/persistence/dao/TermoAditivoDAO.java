package br.cefetrj.sisgee.persistence.dao;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.cefetrj.sisgee.model.entity.TermoAditivo;

/**
 * Classe Respons�vel pelo acesso ao dado da entidade 
 * TermoAditivo 
 *
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class TermoAditivoDAO extends GenericDAO<TermoAditivo> {
	/**
	 * Construtor padr�o de TermoAditivoDAO
	 * 
	 */
	public TermoAditivoDAO() {
		super(TermoAditivo.class, PersistenceManager.getEntityManager());
	}
	
	public TermoAditivo buscarTermosAditivosPorIdAluno(Long idAluno) {
		TypedQuery<TermoAditivo> typedQuery = manager.createQuery("select t from TermoAditivo t where t.termoEstagio.aluno.idAluno = :idAluno", TermoAditivo.class);
		typedQuery.setParameter("idAluno", idAluno);
		try {
			return typedQuery.getSingleResult();	
		} catch (NoResultException e) {
			return null;
		}
	}
}
