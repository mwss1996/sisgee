package br.cefetrj.sisgee.persistence.dao;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.cefetrj.sisgee.model.entity.Aluno;

/**
 * Classe Respons�vel pelo acesso ao dado da entidade Aluno
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class AlunoDAO extends GenericDAO<Aluno> {
	
	/**
	 * Construtor padr�o de AlunoDAO
	 * 
	 */

	public AlunoDAO() {
		super(Aluno.class, PersistenceManager.getEntityManager());
	}
	

	/**
	 * Busca Aluno por Matricula
	 * 
	 * @return Aluno
	 */

	public Aluno buscarAlunoPorMatricula(String matricula) {
		TypedQuery<Aluno> typedQuery = manager.createQuery("select a from Aluno a where a.matricula = :matricula", Aluno.class);
		typedQuery.setParameter("matricula", matricula);
		try {
			return typedQuery.getSingleResult();	
		} catch (NoResultException e) {
			return null;
		}
	}
}
