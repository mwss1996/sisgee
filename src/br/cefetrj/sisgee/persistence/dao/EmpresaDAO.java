package br.cefetrj.sisgee.persistence.dao;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.cefetrj.sisgee.model.entity.Empresa;

/**
 * Classe Respons�vel pelo acesso ao dado da entidade Empresa
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class EmpresaDAO extends GenericDAO<Empresa> {
	/**
	 * Construtor padr�o de EmpresaDAO
	 * 
	 */
	public EmpresaDAO() {
		super(Empresa.class, PersistenceManager.getEntityManager());
	}
	
	public Empresa buscarEmpresaPorCnpj(String cnpj) {
		TypedQuery<Empresa> typedQuery = manager.createQuery("select e from Empresa e where e.cnpjEmpresa = :cnpjEmpresa", Empresa.class);
		typedQuery.setParameter("cnpjEmpresa", cnpj);
		try {
			return typedQuery.getSingleResult();	
		} catch (NoResultException e) {
			return null;
		}
	}
}
