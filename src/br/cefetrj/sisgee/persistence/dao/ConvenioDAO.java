package br.cefetrj.sisgee.persistence.dao;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.cefetrj.sisgee.model.entity.Convenio;

/**
 * Classe Respons�vel pelo acesso ao dado da entidade Convenio
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class ConvenioDAO extends GenericDAO<Convenio> {
	/**
	 * Construtor padr�o de ConvenioDAO
	 * 
	 */
	public ConvenioDAO() {
		super(Convenio.class, PersistenceManager.getEntityManager());
	}
	
	public Convenio buscarConvenioPorNumero(String numeroConvenio) {
		TypedQuery<Convenio> typedQuery = manager.createQuery("select c from Convenio c where c.numeroConvenio = :numeroConvenio", Convenio.class);
		typedQuery.setParameter("numeroConvenio", numeroConvenio);
		try {
			return typedQuery.getSingleResult();	
		} catch (NoResultException e) {
			return null;
		}
	}
}
