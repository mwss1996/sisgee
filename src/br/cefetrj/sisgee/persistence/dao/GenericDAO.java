package br.cefetrj.sisgee.persistence.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * Classe Responsável pelo acesso generico a todas entidades
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class GenericDAO<T> {
	
	protected EntityManager manager;
	private Class<T> t;
	
	GenericDAO(Class<T> t, EntityManager manager){
		this.t = t;
		this.manager = manager;
	}
	
	public List<T> buscarTodos() {
		
		@SuppressWarnings("unchecked")
		List<T> lista = manager.createQuery("select t from " + t.getName() + " t", t).getResultList();
		return lista;
	}
	
	public T buscar(Long id){
		return manager.find(t, id);
	}
	
	public void incluir(T entidade){
       EntityTransaction t = manager.getTransaction();
       t.begin();
       manager.persist(entidade);
       manager.flush();
       t.commit();
	}
	
	public void excluir(T entidade){
		manager.remove(entidade);
	}
	
	public void alterar(T entidade){
		manager.merge(entidade);
	}
	
	public EntityManager getManager(){
		return manager;
	}
	
	public void close() {
		manager.close();
	}
}

