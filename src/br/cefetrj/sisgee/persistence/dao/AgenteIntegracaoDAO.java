package br.cefetrj.sisgee.persistence.dao;

import br.cefetrj.sisgee.model.entity.AgenteIntegracao;

/**
 * Classe Respons�vel pelo acesso ao dado da entidade
 * AgenteIntegracao
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class AgenteIntegracaoDAO extends GenericDAO<AgenteIntegracao> {
	
	/**
	 * Construtor padr�o de AgenteIntegracaoDAO
	 * 
	 */

	public AgenteIntegracaoDAO() {
		super(AgenteIntegracao.class, PersistenceManager.getEntityManager());
	}
}
