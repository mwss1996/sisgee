package br.cefetrj.sisgee.persistence.dao;

import br.cefetrj.sisgee.model.entity.Curso;

/**
 * Classe Respons�vel pelo acesso ao dado da entidade Curso
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class CursoDAO extends GenericDAO<Curso> {
	/**
	 * Construtor padr�o de CursoDAO
	 * 
	 */
	public CursoDAO() {
		super(Curso.class, PersistenceManager.getEntityManager());
	}
	
	/*public List<Curso> buscarCursoPelaVigenciaTermoEstagio(Date dataInicioTermoEstagio, Date dataFimTermoEstagio) {
		String query =	  "select c from TermoEstagio t "
						+ "inner join t.Aluno a with t.aluno = a "
						+ "inner join a.Curso c with a.curso = c "
						+ "where t.dataInicioTermoEstagio > :dataInicioTermoEstagio and t.dataFimTermoEstagio < :dataFimTermoEstagio";
		TypedQuery<Curso> typedQuery = manager.createQuery(query, Curso.class);
		typedQuery.setParameter("dataInicioTermoEstagio", dataInicioTermoEstagio);
		typedQuery.setParameter("dataFimTermoEstagio", dataFimTermoEstagio);
		return typedQuery.getResultList();
	}*/
}