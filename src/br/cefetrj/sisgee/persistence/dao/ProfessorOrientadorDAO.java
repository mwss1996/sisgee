package br.cefetrj.sisgee.persistence.dao;

import br.cefetrj.sisgee.model.entity.ProfessorOrientador;

/**
 * Classe Respons�vel pelo acesso ao dado da entidade 
 * ProfessorOrientador
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class ProfessorOrientadorDAO extends GenericDAO<ProfessorOrientador> {
	
	/**
	 * Construtor padr�o de ProfessorOrientadorDAO
	 * 
	 */
	public ProfessorOrientadorDAO() {
		super(ProfessorOrientador.class, PersistenceManager.getEntityManager());
	}
}