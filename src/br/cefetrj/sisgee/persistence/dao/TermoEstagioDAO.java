package br.cefetrj.sisgee.persistence.dao;

import javax.persistence.TypedQuery;

import br.cefetrj.sisgee.model.entity.TermoEstagio;

/**
 * Classe Respons�vel pelo acesso ao dado da entidade 
 * TermoEstagio 
 *
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class TermoEstagioDAO extends GenericDAO<TermoEstagio> {
	/**
	 * Construtor padr�o de TermoEstagioDAO
	 * 
	 */
	public TermoEstagioDAO() {
		super(TermoEstagio.class, PersistenceManager.getEntityManager());
	}
	
	public TermoEstagio buscarTermosEstagioPorIdAluno(Long idAluno) {
		TypedQuery<TermoEstagio> typedQuery = manager.createQuery("select t from TermoEstagio t where t.aluno.idAluno = :idAluno", TermoEstagio.class);
		typedQuery.setParameter("idAluno", idAluno);
		return typedQuery.getResultList().get(0);
	}
}
