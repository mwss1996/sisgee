
package br.cefetrj.sisgee.commands;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.cefetrj.sisgee.model.entity.TermoEstagio;
import br.cefetrj.sisgee.persistence.dao.TermoEstagioDAO;

/**
 * Command para Registrar Recisao
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class RegistrarRescisao implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getMethod().equals("POST")) {
			Long idAluno = Long.parseLong(request.getParameter("idAluno"));
			TermoEstagioDAO termoEstagioDAO = new TermoEstagioDAO();
			TermoEstagio termoEstagio = termoEstagioDAO.buscarTermosEstagioPorIdAluno(idAluno);

			String dataRescisaoParameter = request.getParameter("dataRescisao");
			
			DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			LocalDate dataRescisao = LocalDate.parse(dataRescisaoParameter, dateTimeFormatter);

			Date dateRescisao = Date.from(dataRescisao.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
			
			termoEstagio.setDataRescisaoTermoEstagio(dateRescisao);
			
			termoEstagioDAO.incluir(termoEstagio);
			request.setAttribute("message", "Recis�o cadastrada com sucesso.");
		}
		request.getRequestDispatcher("/WEB-INF/jsp/termo/registrarRescisao.jsp").forward(request, response);
	}

}