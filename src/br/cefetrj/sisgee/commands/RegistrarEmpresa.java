package br.cefetrj.sisgee.commands;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.cefetrj.sisgee.model.entity.AgenteIntegracao;
import br.cefetrj.sisgee.model.entity.Empresa;
import br.cefetrj.sisgee.persistence.dao.AgenteIntegracaoDAO;
import br.cefetrj.sisgee.persistence.dao.EmpresaDAO;

/**
 * Command para Registrar Empresa
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class RegistrarEmpresa implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getMethod().equals("POST")) {
			
			String isAgenteIntegracao = request.getParameter("isAgenteIntegracao");
			String cnpjEmpresa = request.getParameter("cnpjEmpresa");
			String razaoSocialEmpresa = request.getParameter("razaoSocialEmpresa");
			
			if (cnpjEmpresa != null && !cnpjEmpresa.equals("") && razaoSocialEmpresa != null && !razaoSocialEmpresa.equals("")) {
				if (isAgenteIntegracao.equals("1")) {
					AgenteIntegracao agenteIntegracao = new AgenteIntegracao(cnpjEmpresa, razaoSocialEmpresa);
					AgenteIntegracaoDAO agenteIntegracaoDAO = new AgenteIntegracaoDAO();
					agenteIntegracaoDAO.incluir(agenteIntegracao);
					request.setAttribute("message", "Empresa cadastrada com sucesso.");
				} else if (isAgenteIntegracao.equals("0")) {
					Empresa empresa = new Empresa(cnpjEmpresa, razaoSocialEmpresa);
					String idAgenteIntegracao = request.getParameter("razaoSocialAgenteIntegracao");
					
					AgenteIntegracaoDAO agenteIntegracaoDAO = new AgenteIntegracaoDAO();
					
					AgenteIntegracao agenteIntegracao = agenteIntegracaoDAO.buscar(Long.parseLong(idAgenteIntegracao));
					
					empresa.setAgenteIntegracao(agenteIntegracao);
					EmpresaDAO empresaDAO = new EmpresaDAO();
					empresaDAO.incluir(empresa);
					request.setAttribute("message", "Agente de Integracao cadastrado com sucesso.");
				}
			} else {
				request.setAttribute("message", "Preencha os campos.");
			}
		}
		AgenteIntegracaoDAO agenteIntegracaoDAO = new AgenteIntegracaoDAO();
		request.setAttribute("listAgenteIntegracao", agenteIntegracaoDAO.buscarTodos());
		request.getRequestDispatcher("/WEB-INF/jsp/empresa/registrarEmpresa.jsp").forward(request, response);
	}
}