package br.cefetrj.sisgee.commands;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.cefetrj.sisgee.model.entity.Aluno;
import br.cefetrj.sisgee.model.entity.Curso;
import br.cefetrj.sisgee.model.entity.TermoAditivo;
import br.cefetrj.sisgee.model.entity.TermoEstagio;
import br.cefetrj.sisgee.persistence.dao.CursoDAO;

/**
 * Command para Exibir RelatorioConsolidado
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class RelatorioConsolidado implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getMethod().equals("POST")) {
			/*String dataInicioConvenioParameter = request.getParameter("dataInicio");
			String dataFimConvenioParameter = request.getParameter("dataTermino");
			
			DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			LocalDate dataInicioConvenio = LocalDate.parse(dataInicioConvenioParameter, dateTimeFormatter);
			LocalDate dataFimConvenio = LocalDate.parse(dataFimConvenioParameter, dateTimeFormatter);
			
			Date dateInicioConvenio = Date.from(dataInicioConvenio.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
			Date dateFimConvenio = Date.from(dataFimConvenio.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());*/
			
			CursoDAO cursoDAO = new CursoDAO();
			//List<Curso> cursos = cursoDAO.buscarCursoPelaVigenciaTermoEstagio(dateInicioConvenio, dateFimConvenio);
			List<Curso> cursos = cursoDAO.buscarTodos();
			List<Relatorio> relatorios = new ArrayList<>();
			
			for (Curso curso : cursos) {
				
				int termosDeEstagioRegistrados = 0;
				int aditivosRegistrados = 0;
				int recisoesRegistradas = 0;

				if (curso.getAlunos() != null) {
					for (Aluno aluno : curso.getAlunos()) {
						if (aluno.getTermosEstagios() != null) {
							for (TermoEstagio termoEstagio : aluno.getTermosEstagios()) {
								termosDeEstagioRegistrados++;
								if (termoEstagio.getDataRescisaoTermoEstagio() != null) {
									recisoesRegistradas++;
								}
								if (termoEstagio.getTermosAditivos() != null) {
									for (TermoAditivo termoAditivo : termoEstagio.getTermosAditivos()) {
										aditivosRegistrados++;
									}
								}
							}
						}
					}
				}
				
				relatorios.add(new Relatorio(curso.getNomeCurso(), termosDeEstagioRegistrados, aditivosRegistrados, recisoesRegistradas));
			}
			request.setAttribute("relatorios", relatorios);
		}
		request.getRequestDispatcher("/WEB-INF/jsp/termo/relatorioConsolidado.jsp").forward(request, response);
	}
	
	public class Relatorio {
		
		String curso;
		int termosDeEstagioRegistrados;
		int aditivosRegistrados;
		int recisoesRegistradas;
		
		public Relatorio(String curso, int termosDeEstagioRegistrados, int aditivosRegistrados, int recisoesRegistradas) {
			this.curso = curso;
			this.termosDeEstagioRegistrados = termosDeEstagioRegistrados;
			this.aditivosRegistrados = aditivosRegistrados;
			this.recisoesRegistradas = recisoesRegistradas;
		}

		public String getCurso() {
			return curso;
		}

		public void setCurso(String curso) {
			this.curso = curso;
		}

		public int getTermosDeEstagioRegistrados() {
			return termosDeEstagioRegistrados;
		}

		public void setTermosDeEstagioRegistrados(int termosDeEstagioRegistrados) {
			this.termosDeEstagioRegistrados = termosDeEstagioRegistrados;
		}

		public int getAditivosRegistrados() {
			return aditivosRegistrados;
		}

		public void setAditivosRegistrados(int aditivosRegistrados) {
			this.aditivosRegistrados = aditivosRegistrados;
		}

		public int getRecisoesRegistradas() {
			return recisoesRegistradas;
		}

		public void setRecisoesRegistradas(int recisoesRegistradas) {
			this.recisoesRegistradas = recisoesRegistradas;
		}
	}
}