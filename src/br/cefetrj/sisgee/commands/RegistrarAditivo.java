package br.cefetrj.sisgee.commands;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.cefetrj.sisgee.model.entity.TermoAditivo;
import br.cefetrj.sisgee.model.entity.TermoEstagio;
import br.cefetrj.sisgee.persistence.dao.TermoAditivoDAO;
import br.cefetrj.sisgee.persistence.dao.TermoEstagioDAO;

/**
 * Command para Registrar TermoAditivo
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class RegistrarAditivo implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getMethod().equals("POST")) {
			Long idAluno = Long.parseLong(request.getParameter("idAluno"));
			TermoEstagioDAO termoEstagioDAO = new TermoEstagioDAO();
			TermoEstagio termoEstagio = termoEstagioDAO.buscarTermosEstagioPorIdAluno(idAluno);
			
			Short cargaHoraria = Short.parseShort(request.getParameter("cargaHoraria"));
			Float valorBolsa = Float.parseFloat(request.getParameter("valorBolsa").replace(".", "").replace(",", "."));
			
			String enderecoTermoAditivo = request.getParameter("enderecoTermoAditivo");
			String numeroEnderecoTermoAditivo = request.getParameter("numeroEnderecoTermoAditivo");
			String complementoEnderecoTermoAditivo = request.getParameter("complementoEnderecoTermoAditivo");
			String bairroEnderecoTermoAditivo = request.getParameter("bairroEnderecoTermoAditivo");
			String cepEnderecoTermoAditivo = request.getParameter("cepEnderecoTermoAditivo");
			String cidadeEnderecoTermoAditivo = request.getParameter("cidadeEnderecoTermoAditivo");
			String estadoEnderecoTermoAditivo = request.getParameter("estadoEnderecoTermoAditivo");
			
			TermoAditivo termoAditivo = new TermoAditivo();
			
			termoAditivo.setTermoEstagio(termoEstagio);
			
			termoAditivo.setCargaHorariaTermoAditivo(cargaHoraria);
			termoAditivo.setValorBolsaTermoAditivo(valorBolsa);
			termoAditivo.setEnderecoTermoAditivo(enderecoTermoAditivo);
			termoAditivo.setNumeroEnderecoTermoAditivo(numeroEnderecoTermoAditivo);
			termoAditivo.setComplementoEnderecoTermoEstagio(complementoEnderecoTermoAditivo);
			termoAditivo.setBairroEnderecoTermoAditivo(bairroEnderecoTermoAditivo);
			termoAditivo.setCidadeEnderecoTermoAditivo(cidadeEnderecoTermoAditivo);
			termoAditivo.setEstadoEnderecoTermoAditivo(estadoEnderecoTermoAditivo);
			termoAditivo.setCepEnderecoTermoAditivo(cepEnderecoTermoAditivo);
			
			TermoAditivoDAO termoAditivoDAO = new TermoAditivoDAO();
			termoAditivoDAO.incluir(termoAditivo);
			request.setAttribute("message", "Termo Aditivo cadastrado com sucesso.");
		}
		request.getRequestDispatcher("/WEB-INF/jsp/termo/registrarAditivo.jsp").forward(request, response);
	}
}