package br.cefetrj.sisgee.commands;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import br.cefetrj.sisgee.model.entity.Empresa;
import br.cefetrj.sisgee.persistence.dao.EmpresaDAO;

/**
 * Command para buscar a entidade Empresa Ligada a um Agente
 * Integracao
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */


public class BuscarEmpresaLigadaAgenteIntegracao implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
		EmpresaDAO empresaDAO = new EmpresaDAO();
		String cnpjEmpresa = request.getParameter("cnpjEmpresa");
		Empresa empresa = empresaDAO.buscarEmpresaPorCnpj(cnpjEmpresa);
		String temp = request.getParameter("idAgenteIntegracao");
		Long idAgenteIntegracao = Long.parseLong(temp);
		if (empresa.getAgenteIntegracao().getIdAgenteIntegracao() == idAgenteIntegracao) {
			ObjectMapper objectMapper = new ObjectMapper();
			String jsonString = objectMapper.writeValueAsString(empresa);

			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(jsonString);
		} else {
			response.setStatus(500);
		}
	}
}
