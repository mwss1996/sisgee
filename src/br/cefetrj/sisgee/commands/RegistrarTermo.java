package br.cefetrj.sisgee.commands;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.cefetrj.sisgee.model.entity.Aluno;
import br.cefetrj.sisgee.model.entity.Convenio;
import br.cefetrj.sisgee.model.entity.Empresa;
import br.cefetrj.sisgee.model.entity.ProfessorOrientador;
import br.cefetrj.sisgee.model.entity.TermoEstagio;
import br.cefetrj.sisgee.persistence.dao.AgenteIntegracaoDAO;
import br.cefetrj.sisgee.persistence.dao.AlunoDAO;
import br.cefetrj.sisgee.persistence.dao.ConvenioDAO;
import br.cefetrj.sisgee.persistence.dao.EmpresaDAO;
import br.cefetrj.sisgee.persistence.dao.ProfessorOrientadorDAO;
import br.cefetrj.sisgee.persistence.dao.TermoEstagioDAO;

/**
 * Command para Registrar Termo
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class RegistrarTermo implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getMethod().equals("POST")) {
			String numeroConvenio = request.getParameter("numeroConvenio");
			String isAgenteIntegracao = request.getParameter("isAgenteIntegracao");
			Long idEmpresa = null;
			if (isAgenteIntegracao.equals("1")) {
				String idEmpresaLigadaAgenteIntegracao = request.getParameter("idEmpresaLigadaAgenteIntegracao");
				idEmpresa = Long.parseLong(idEmpresaLigadaAgenteIntegracao);
				
			} else if (isAgenteIntegracao.equals("0")) {
				String idEmpresaParameter = request.getParameter("idEmpresa");
				idEmpresa = Long.parseLong(idEmpresaParameter);
			}
			EmpresaDAO empresaDAO = new EmpresaDAO();
			Empresa empresa = empresaDAO.buscar(idEmpresa);
			
			String dataInicioConvenioParameter = request.getParameter("dataInicio");
			String dataFimConvenioParameter = request.getParameter("dataTermino");
			
			DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			LocalDate dataInicioConvenio = LocalDate.parse(dataInicioConvenioParameter, dateTimeFormatter);
			LocalDate dataFimConvenio = LocalDate.parse(dataFimConvenioParameter, dateTimeFormatter);

			Date dateInicioConvenio = Date.from(dataInicioConvenio.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
			Date dateFimConvenio = Date.from(dataFimConvenio.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
			
			ConvenioDAO convenioDAO = new ConvenioDAO();
			Convenio convenio = convenioDAO.buscarConvenioPorNumero(numeroConvenio);
			if (convenio == null) {
				convenio = new Convenio(numeroConvenio, dateInicioConvenio, dateFimConvenio, empresa);
				convenioDAO.incluir(convenio);
				convenio = convenioDAO.buscarConvenioPorNumero(numeroConvenio);
			} else {
				convenio.setDataInicioConvenio(dateInicioConvenio);
				convenio.setDataFimConvenio(dateFimConvenio);
				convenio.setEmpresa(empresa);
			}
			
			AlunoDAO alunoDAO = new AlunoDAO();
			Long idAluno = Long.parseLong(request.getParameter("idAluno"));
			Aluno aluno = alunoDAO.buscar(idAluno);

			Short cargaHoraria = Short.parseShort(request.getParameter("cargaHoraria"));
			Float valorBolsa = Float.parseFloat(request.getParameter("valorBolsa").replace(".", "").replace(",", "."));
			
			ProfessorOrientador professorOrientador = null;
			String isEstagioObrigatorio = request.getParameter("isEstagioObrigatorio");
			Short eEstagioObrigatorio = Short.parseShort(isEstagioObrigatorio);
			if (eEstagioObrigatorio == 1) {
				ProfessorOrientadorDAO professorOrientadorDAO = new ProfessorOrientadorDAO();
				Long idProfessorOrientador = Long.parseLong(request.getParameter("professorOrientador"));
				professorOrientador = professorOrientadorDAO.buscar(idProfessorOrientador);
			}
			
			String enderecoTermoEstagio = request.getParameter("enderecoTermoEstagio");
			String numeroEnderecoTermoEstagio = request.getParameter("numeroEnderecoTermoEstagio");
			String complementoEnderecoTermoEstagio = request.getParameter("complementoEnderecoTermoEstagio");
			String bairroEnderecoTermoEstagio = request.getParameter("bairroEnderecoTermoEstagio");
			String cepEnderecoTermoEstagio = request.getParameter("cepEnderecoTermoEstagio");
			String cidadeEnderecoTermoEstagio = request.getParameter("cidadeEnderecoTermoEstagio");
			String estadoEnderecoTermoEstagio = request.getParameter("estadoEnderecoTermoEstagio");
			
			TermoEstagio termoEstagio = new TermoEstagio();
			termoEstagio.setDataInicioTermoEstagio(dateInicioConvenio);
			termoEstagio.setDataFimTermoEstagio(dateFimConvenio);
			termoEstagio.setSituacaoTermoEstagio("Regular");
			termoEstagio.setConvenio(convenio);
			termoEstagio.setAluno(aluno);
			termoEstagio.setCargaHorariaTermoEstagio(cargaHoraria);
			termoEstagio.setValorBolsa(valorBolsa);
			termoEstagio.setEnderecoTermoEstagio(enderecoTermoEstagio);
			termoEstagio.setNumeroEnderecoTermoEstagio(numeroEnderecoTermoEstagio);
			termoEstagio.setComplementoEnderecoTermoEstagio(complementoEnderecoTermoEstagio);
			termoEstagio.setBairroEnderecoTermoEstagio(bairroEnderecoTermoEstagio);
			termoEstagio.setCidadeEnderecoTermoEstagio(cidadeEnderecoTermoEstagio);
			termoEstagio.setEstadoEnderecoTermoEstagio(estadoEnderecoTermoEstagio);
			termoEstagio.setCepEnderecoTermoEstagio(cepEnderecoTermoEstagio);
			termoEstagio.seteEstagioObrigatorio(eEstagioObrigatorio);
			termoEstagio.setProfessorOrientador(professorOrientador);
			
			TermoEstagioDAO termoEstagioDAO = new TermoEstagioDAO();
			termoEstagioDAO.incluir(termoEstagio);
			request.setAttribute("message", "Termo de Est�gio cadastrado com sucesso.");
		}
		ProfessorOrientadorDAO professorOrientadorDAO = new ProfessorOrientadorDAO();
		AgenteIntegracaoDAO agenteIntegracaoDAO = new AgenteIntegracaoDAO();
		request.setAttribute("listAgenteIntegracao", agenteIntegracaoDAO.buscarTodos());
		request.setAttribute("listProfessorOrientador", professorOrientadorDAO.buscarTodos());
		request.getRequestDispatcher("/WEB-INF/jsp/termo/registrarTermo.jsp").forward(request, response);
	}
}
