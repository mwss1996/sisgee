package br.cefetrj.sisgee.commands;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public interface Command {

	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
	
}