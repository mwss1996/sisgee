package br.cefetrj.sisgee.commands;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import br.cefetrj.sisgee.model.entity.Empresa;
import br.cefetrj.sisgee.persistence.dao.EmpresaDAO;


/**
 * Command para buscar a entidade Empresa
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class BuscarEmpresa implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
		EmpresaDAO empresaDAO = new EmpresaDAO();
		String cnpjEmpresa = request.getParameter("cnpjEmpresa");
		Empresa empresa = empresaDAO.buscarEmpresaPorCnpj(cnpjEmpresa);
		
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = objectMapper.writeValueAsString(empresa);

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(jsonString);
	}
}