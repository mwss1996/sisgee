package br.cefetrj.sisgee.commands;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import br.cefetrj.sisgee.model.entity.Aluno;
import br.cefetrj.sisgee.persistence.dao.AlunoDAO;


/**
 * Command para buscar a entidade Aluno
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class BuscarAluno implements Command {
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
		AlunoDAO alunoDAO = new AlunoDAO();
		String matriculaAluno = request.getParameter("matriculaAluno");
		Aluno aluno = alunoDAO.buscarAlunoPorMatricula(matriculaAluno);
		
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = objectMapper.writeValueAsString(aluno);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(jsonString);
	}
}